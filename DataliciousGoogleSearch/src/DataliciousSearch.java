import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class DataliciousSearch {

	public static void main(String[] args) throws InterruptedException {
		
		
		//Task #1
		String exeChromePath = "F:\\Chrome\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exeChromePath);
		WebDriver driver = new ChromeDriver();
		driver.navigate().to("http://www.google.com");
		driver.manage().window().maximize();
		
		driver.findElement(By.id("lst-ib")).sendKeys("Datalicious");
		driver.findElement(By.id("lst-ib")).sendKeys(Keys.ENTER);
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("resultStats")));
		driver.findElement(By.xpath("//a[@href='https://www.datalicious.com/']")).click();
		
		driver.quit();
	}
	
}
